import React from 'react';
import {Image, Text, View, Linking} from 'react-native';
import {ItemCardText, ItemTitle, ItemText, Url, DetailPageItemImg, fontWeightBold, marginTop10 } from "../components/common/Styles";
import Card from "../components/common/Card";

export default class DetailScreen extends React.Component {
    constructor(props) {
        super(props);
    }

    static navigationOptions = ({ navigation }) => {
        const item = navigation.getParam('item_details', {});
        return {
            title: `Details: ${item.title}`,
        };
    };

    render() {
        const { navigation } = this.props;
        const item = navigation.getParam('item_details', {});

        return (
                <Card>
                    <Image
                        source={{ uri: item.image_url }}
                        style={ DetailPageItemImg }
                        resizeMode="cover"
                    />
                    <View style={ ItemCardText }>
                        <Text style={ ItemTitle }> { item.title }</Text>
                        <Text style={ ItemText }>Publisher: {item.publisher}</Text>
                        <Text style={ [ItemText,fontWeightBold,marginTop10 ]}>For More Info Click the link below</Text>
                        <Text style={ Url } onPress = { () => Linking.openURL(item.source_url)}>{item.source_url}</Text>
                    </View>
                </Card>
        );
    }
}
