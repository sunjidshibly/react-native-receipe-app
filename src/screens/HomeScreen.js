import React from 'react';
import { View, ActivityIndicator, FlatList, SafeAreaView} from 'react-native';
import ItemList from "../components/ItemList";
import {SearchBar} from 'react-native-elements';



export default class HomeScreen extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            data: null,
            loading:false,
        };
    }

    componentWillMount(){
        this.setState( { loading:true });
        fetch('https://www.food2fork.com/api/search?key=02f376d946d0057dbbb2c7bdb5cfb9c7')
            .then( (response) => {
                return response.json() })
            .then( (responseJson) => {
                this.setState( {
                    data: responseJson.recipes,
                    loading: false,
                    search:''
                } );
                this.arrayholder = responseJson.recipes;
            } )
            .catch( (error)  => this.setState({ loading: false}))
    };

    static navigationOptions = {
        title: 'Receipe List',
    };
    keyExtractor = item => item.recipe_id;

    renderItem = ({item}) => {
        return <ItemList item={item} navigation={this.props.navigation} />
    };

    searchFunction = (search) =>{
        this.setState( {search} )

        const newData = this.arrayholder.filter(item => {
            const title = item.title.toUpperCase();
            const textData = search.toUpperCase();
            return title.indexOf(textData) > -1;
        });

        this.setState({
            data: newData,
        });
    };

    renderSearchBox = () =>{
      return  (
          <SearchBar
              placeholder = "Search By Title..."
              lightTheme={true}
              autoCorrect={false}
              round
              onChangeText={search => this.searchFunction(search)}
              value ={this.state.search}
          />
      );
    };

    render() {
        const { loading, data } = this.state;

        if(loading){
            return(
                <View style = {{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                    <ActivityIndicator size="large" color="#0000ff" />
                </View>
            )
        }
        return (
            <SafeAreaView style = {{ flex: 1}} >
                <FlatList
                    data = { data }
                    renderItem={this.renderItem}
                    keyExtractor = { this.keyExtractor }
                    ListHeaderComponent = { this.renderSearchBox }
                />
            </SafeAreaView>
        );
    }
}
