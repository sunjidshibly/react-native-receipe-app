import React, { Component } from 'react';
import {View} from 'react-native';
import {CardStyle} from "./Styles";

export default class Card extends Component {
    render() {
        return <View style={ CardStyle }>{this.props.children}</View>;
    }
}