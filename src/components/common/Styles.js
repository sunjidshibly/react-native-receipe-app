const CardStyle = {
        margin: 7,
        borderWidth: 1,
        borderColor: '#ddd',
        borderRadius:8,
        shadowColor:'#000',
        shadowOffset: { width: 0, height:4 },
        shadowOpacity: 1,
        shadowRadius: 4,
        backgroundColor: '#bfd1d561'
    }

const Url = {
    fontSize: 12,
    color: '#4b10ea',
    textDecorationLine: 'underline',
}

const ItemText =  {
    fontSize: 13,
        color: '#303030',
}

const ItemTitle = {
    fontSize: 15,
    color: '#303030',
    fontWeight: 'bold'
}

const ItemCardText = {
    marginTop: 10,
    marginLeft: 10,
    marginBottom: 15
}

const ItemImg = { height: 120,
    flex: 1,
    borderRadius: 8,
    margin:5
}

const DetailPageItemImg = {
    height: 200,
    borderRadius: 8,
    margin:5
}

const fontWeightBold = {
    fontWeight: 'bold',
}

const marginTop10 = {
    marginTop: 10,
}

export  { CardStyle, Url, ItemText, ItemTitle, ItemCardText, ItemImg, DetailPageItemImg, fontWeightBold, marginTop10 };