import React, {Component} from 'react';
import {Image, Text, TouchableOpacity, View} from "react-native";
import {ItemCardText, ItemTitle, ItemText, ItemImg, CardStyle} from "./common/Styles";

class ItemList extends Component {
    render() {
        const { item, navigation } = this.props;
        return (
            <TouchableOpacity
                onPress={ () => navigation.navigate('Details',{item_details: item }) }
                style={CardStyle}
            >
                <Image
                    source={{ uri: item.image_url }}
                    style={ ItemImg }
                    resizeMode="cover"
                />
                <View style={ ItemCardText }>
                    <Text style={ ItemTitle }> { item.title }</Text>
                    <Text style={ ItemText }> { item.publisher }</Text>
                </View>

            </TouchableOpacity>

        );
    }
}

export default ItemList;